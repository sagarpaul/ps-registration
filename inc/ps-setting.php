<?php

/**
 * WordPress settings API demo class
 *
 * @author Sagar Paul
 */

if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('Ps_Setting')):

    class Ps_Setting {

        private $settings_api;

        /**
         * Holds the class object.
         *
         * @since 1.0
         *
         */
        public static $_instance;

        /**
         * Hold the base Object
         *
         * @since 1.0
         */

        public $base;

        /**
         * Load Construct
         *
         * @since 1.0
         *
         */

        public function __construct() {
            $this->settings_api = new Ps_Setting_Class();
            add_action('admin_init', array($this, 'admin_init'));
            add_action('admin_menu', array($this, 'admin_menu'));
        }
        public function admin_init() {

            //set the settings
            $this->settings_api->set_sections($this->get_settings_sections());
            $this->settings_api->set_fields($this->get_settings_fields());

            //initialize settings
            $this->settings_api->admin_init();
        }
        public function admin_menu() {
            add_menu_page('Front-End User Login and Registration', 'Ps Registration','manage_options', 'ps-registration-setting',array($this, 'ps_plugin_page'));
        }
        public function get_settings_sections() {
            $sections = array(
                array(
                    'id' => 'ps_basic_setting',
                    'title' => esc_html__('Basic Settings', 'ps_registration'),
                    'desc' => esc_html__('Use This Shortcode [ps_registration]', 'ps_registration'),
                ),
                array(
                    'id' => 'ps_feild_setting',
                    'title' => esc_html__('Feild Settings', 'ps_registration'),
                ),
            );
            return $sections;
        }
        /**
         * Returns all the settings fields
         *
         * @return array settings fields
         */
        public function get_settings_fields() {


            $settings_fields = array(
                    'ps_feild_setting' => array(
                        array(
                            'name' => 'ps_password',
                            'label' => esc_html__('Password', 'ps_registration'),
                            'desc'  => esc_html__('Enable','ps_registration'),
                            'type' => 'checkbox',
                            'default' => 'yes',
                        ),
                        array(
                            'name' => 'ps_first_name',
                            'label' => esc_html__('First Name', 'ps_registration'),
                            'type' => 'multicheck',
                            'options'   => array(
                                'yes' => esc_html__('Enable','ps_registration'),
                                'ps-required' => esc_html__('Make As Required','ps_registration')
                            ),
                        ),
                        array(
                            'name' => 'ps_last_name',
                            'label' => esc_html__('Last Name', 'ps_registration'),
                            'type' => 'multicheck',
                            'options'   => array(
                                'yes' => esc_html__('Enable','ps_registration'),
                                'ps-required' => esc_html__('Make As Required','ps_registration')
                            ),
                        ),
                        
                    ),
                    'ps_basic_setting' => array(
                        array(
                            'name' => 'ps_register_redirect',
                            'label' => esc_html__('Redirect after registration', 'ps_registration'),
                            'type' => 'select',
                            'options' => $this->get_pages(),
                            'default' => '',
                        ),
                        array(
                            'name' => 'ps_login_redirect',
                            'label' => esc_html__('Redirect after login', 'ps_registration'),
                            'type' => 'select',
                            'options' => $this->get_pages(),
                            'default' => '',
                        ), 
                        array(
                            'name' => 'ps_logut_redirect',
                            'label' => esc_html__('Redirect after logout', 'ps_registration'),
                            'type' => 'select',
                            'options' => $this->get_pages(),
                            'default' => '',
                        ),
                    ) 
                );
            return $settings_fields;
        }
        public function ps_plugin_page() {
            echo '<div class="wrap bs_Yellow">';
            $this->settings_api->show_navigation();
            $this->settings_api->show_forms();
            echo '</div>';
        }
        /**
         * Get all the pages
         *
         * @return array page names with key value pairs
         */
        public function get_pages() {
            $pages = get_pages();
            $pages_options = array();
            $pages_options = array('' => esc_html__('Select Page','ps_registration') );
            if ($pages) {
                foreach ($pages as $page) {
                    $pages_options[$page->ID] = $page->post_title;
                }
            }
            return $pages_options;
        }
        /**
         *
         * Get Option Value
         *
         */
        public function ps_get_option($option, $section, $default = '') {
            $options = get_option($section);
            if (isset($options[$option])) {
                return $options[$option];
            }
            return $default;
        }

        /**
         * The Setting Object.
         * @since 1.0
         *
         */
        public static function ps_get_instance() {
            if (!isset($_instance)) {
                self::$_instance = new Ps_Setting();
            }
            return self::$_instance;
        }
    }
    $Ps_Setting = Ps_Setting::ps_get_instance();
endif;