<div id="register" class="ps-registration-form">

    <!-- Register Form -->
    
    <form id="ps_create_user" action="#" method="post" data-redirect-url = "<?php echo esc_url(get_the_permalink($redirect)); ?>" data-redirect-id="<?php echo esc_attr($redirect); ?>">
        <h1> <?php echo esc_html__('Registration','ps_registration'); ?> </h1>
        <div class="form-inner">
            <h1 id="ps_msg"></h1>
            <?php if($show_first_name == 'yes'): ?>
                <p> 
                    <label for="user_first_name" class="uname"><?php echo esc_html__('First name','ps_registration'); ?></label>
                    <input id="user_first_name"  class="<?php echo esc_attr($first_name_required); ?>" name="user_first_name" type="text" placeholder="sagar" />
                </p>
            <?php endif; ?>
            <?php if($show_last_name == 'yes'): ?>    
            <p> 
                <label for="user_last_name" class="uname"><?php echo esc_html__('Last name','ps_registration'); ?></label>
                <input id="user_last_name" class="<?php echo esc_attr($last_name_required); ?>" name="user_last_name" type="text" placeholder="paul" />
            </p>
            <?php endif; ?>
            <p> 
                <label for="user_name" class="uname required"><?php echo esc_html__('Username','ps_registration'); ?></label>
                <input id="user_name" class="ps-required" name="user_name" type="text" placeholder="user" />
            </p>
            <p> 
                <label for="user_email_address" class="youmail required"><?php echo esc_html__('Email','ps_registration'); ?></label>
                <input id="user_email_address" class="ps-required" name="user_email_address"  type="email" placeholder="mymail@mail.com"/> 
            </p>
            <?php if($show_password == 'yes'): ?>
                <p> 
                    <label for="user_password" class="youpasswd required"><?php echo esc_html__('Password','ps_registration'); ?></label>
                    <input id="user_password" class="ps-required" name="user_password"  type="password" placeholder="eg. X8df!90EO"/>
                </p>
            <?php endif; ?>
            <p class="signin button"> 
                <input type="submit" value="Register" id="submit_create_user"/>
            </p>
            <p class="change_link">
                <a href="#" class="ps_reset_switch"><?php echo esc_html__('Reset Password','ps_registration'); ?></a> <?php echo esc_html__('OR','ps_registration'); ?>
                <a href="#" class="ps_login_switch"><?php echo esc_html__('Login','ps_registration'); ?></a>
            </p>
        </div>
    </form>

    <!-- Login Form -->

    <form action="#" id="ps_login_form" method="post" style="display: none;" data-redirect-url = "<?php echo esc_url(get_the_permalink($login_redirect)); ?>" data-redirect-id="<?php echo esc_attr($login_redirect); ?>" > 
        <h1><?php echo esc_html__('Log in','ps_registration'); ?></h1>
        <div class="form-inner">
            <h1 id="ps_login_msg"></h1>
            <p>
                <label for="lu" class="uname"><?php echo esc_html__('Your email or username ','ps_registration'); ?></label>
                <input id="login_user_name" name="user_name" class="ps-required" type="text" placeholder="myusername or mymail@mail.com"/>
            </p>
            <p> 
                <label for="lp" class="youpasswd"><?php echo esc_html__('Your password ','ps_registration'); ?></label>
                <input id="user_pass" name="user_password" class="ps-required" type="password" placeholder="eg. X8df!90EO" /> 
            </p>
            <p class="login button"> 
                <input type="submit" id="ps_login_submit" value="Login" />
            </p>
            <p class="change_link">
                <a href="#" class="ps_register_switch"><?php echo esc_html__('Register','ps_registration'); ?></a> <?php echo esc_html__('OR','ps_registration'); ?>
                <a href="#" class="ps_reset_switch"><?php echo esc_html__('Reset Password','ps_registration'); ?></a>

            </p>
        </div>
    </form>

    <!-- Reset Form -->

    <form action="#" id="ps_reset_password" method="post" style="display: none;">
        <h1><?php echo esc_html__('Reset Password','ps_registration'); ?></h1>
        <div class="form-inner">
            <h1 id="ps_reset_msg"></h1>
            <p>
                <label for="lu" class="uname"><?php echo esc_html__('Your email or username','ps_registration'); ?></label>
                <input id="reset_username" name="reset_username" class="ps-required" type="text" placeholder="myusername or mymail@mail.com"/>
            </p>
            <p class="login button">
                <input type="submit" id="ps_reset_submit" value="Reset" />
            </p>
            <p class="change_link">
                <a href="#" class="ps_login_switch"><?php echo esc_html__('Login','ps_registration'); ?></a> <?php echo esc_html__('OR','ps_registration'); ?>
                <a href="#" class="ps_register_switch"><?php echo esc_html__('Register','ps_registration'); ?></a>
            </p>
        </div>
    </form>
</div>

<div class="login_content" style="display: none" data-logout-url="<?php echo wp_logout_url(get_permalink($logout_redirect)); ?>"></div>