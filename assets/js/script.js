(function($){
    
    'use strict';

	$('.ps_reset_switch').click(function(){
        $('#ps_create_user').hide('slow');
		$('#ps_reset_password').show('slow');
        $('#ps_login_form').hide('slow');
	});

	$('.ps_login_switch').click(function(){
		$('#ps_reset_password').hide('slow');
        $('#ps_create_user').hide('slow');
		$('#ps_login_form').show('slow');
	});

    $('.ps_register_switch').click(function(){
        $('#ps_reset_password').hide('slow');
        $('#ps_create_user').show('slow');
        $('#ps_login_form').hide('slow');
    });

	/**
	 *
	 * Create New User Ajax Call
	 *
	 */

	$('#ps_create_user').on( 'submit', function(e) {
        e.preventDefault();

        var user_first_name 	= $("#user_first_name").val();
        var user_last_name  	= $("#user_last_name").val();
        var user_name 			= $("#user_name").val();
        var user_email_address 	= $("#user_email_address").val();
        var user_password 		= $("#user_password").val();
        var redirect_url        = $(this).data('redirect-url');
        var redirect_id         = $(this).data('redirect-id');
        var required = 0;

        $(".ps-required", this).each(function() {
            if ($(this).val() == '')
            {
                $(this).addClass('reqError');
                required += 1;
            }
            else
            {
                if ($(this).hasClass('reqError'))
                {
                    $(this).removeClass('reqError');
                    if (required > 0)
                    {
                        required -= 1;
                    }
                }
            }
        });
        if (required === 0){
            $("#submit_create_user").val('Processsing...');
            $.ajax({
                url : ps_check_obj.ajaxurl,
                type : 'post',
                dataType : 'Json',
                data : {
                    action : 'ps_create_user',
                    user_first_name : user_first_name,
                    user_last_name : user_last_name,
                    user_name : user_name,
                    user_email_address : user_email_address,
                    user_password : user_password,
                    ps_security : ps_check_obj.ajax_nonce
                },
                success : function( response ) {
                    var response = JSON.parse(JSON.stringify(response));
                    if(response.status == 'success'){
                        $("#submit_create_user").val('Register');
                        if(redirect_id != ''){
                           window.location.href = redirect_url;
                        }else{
                            $('#ps_msg').show().text(response.msg).css('color','green');
                            $('#ps_create_user')[0].reset();
                        }
                    }else{
                        $('#ps_msg').show().text(response.msg).css('color','red');
                        $("#submit_create_user").val('Register');
                    }
                },
                error : function (jqXHR, textStatus, errorThrown) {
                	   console.log(errorThrown);
                }
            });
        }
    });
	
    /**
     *
     * Check user login
     *
     */

    $('#ps_login_form').on( 'submit', function(e) {
        e.preventDefault();

        var user_name           = $("#login_user_name").val();
        var user_password       = $("#user_pass").val();
        var redirect_url        = $(this).data('redirect-url');
        var redirect_id         = $(this).data('redirect-id');
        var required = 0;
        
        $(".ps-required", this).each(function() {
            if ($(this).val() == '')
            {
                $(this).addClass('reqError');
                required += 1;
            }
            else
            {
                if ($(this).hasClass('reqError'))
                {
                    $(this).removeClass('reqError');
                    if (required > 0)
                    {
                        required -= 1;
                    }
                }
            }
        });
        if (required === 0){
            $("#ps_login_submit").val('Processsing...');
            $.ajax({
                url : ps_check_obj.ajaxurl,
                type : 'post',
                dataType : 'Json',
                data : {
                    action : 'ps_login',
                    user_name : user_name,
                    user_password : user_password,
                    ps_security : ps_check_obj.ajax_nonce
                },
                success : function( response ) {
                    var response = JSON.parse(JSON.stringify(response));
                     console.log(response);
                    if(response.status == 'success'){
                        $(".ps-registration-form").hide('fast');
                        $(".login_content").fadeIn('slow');
                        var content = ''+response.avatar+'<h1 id="ps_login_success">Hi '+response.name+'<br><a href="'+ $(".login_content").data('logout-url')+'" class="ps_logout">Logout</h1>';
                        $('.login_content').html(content);
                        if(redirect_id != ''){
                           window.location.href = redirect_url;
                        }else{
                            $('#ps_msg').show().text(response.msg).css('color','green');
                        }
                    }else{
                        $('#ps_login_msg').show().text(response.msg).css('color','red');
                        $("#ps_login_submit").val('Login');
                    }
                },
                error : function (jqXHR, textStatus, errorThrown) {
                       console.log(errorThrown);
                }
            });
        }
    });

    /**
     *
     * Reset Password
     *
     */

    $('#ps_reset_password').on( 'submit', function(e) {
        e.preventDefault();

        var reset_username = $("#reset_username").val();
        var required = 0;
        
        $(".ps-required", this).each(function() {
            if ($(this).val() == '')
            {
                $(this).addClass('reqError');
                required += 1;
            }
            else
            {
                if ($(this).hasClass('reqError'))
                {
                    $(this).removeClass('reqError');
                    if (required > 0)
                    {
                        required -= 1;
                    }
                }
            }
        });
        if (required === 0){
            $("#ps_reset_submit").val('Processsing...');
            $.ajax({
                url : ps_check_obj.ajaxurl,
                type : 'post',
                dataType : 'Json',
                data : {
                    action : 'ps_resetpassword',
                    user_name : reset_username,
                    ps_security : ps_check_obj.ajax_nonce
                },
                success : function( response ) {
                    if(response.status == 'success'){
                        $("#ps_reset_submit").val('Reset');
                            $('#ps_reset_msg').show().text(response.msg).css('color','green');
                            $('#ps_reset_password')[0].reset();
                    }else{
                        $('#ps_reset_msg').show().text(response.msg).css('color','red');
                        $("#ps_reset_submit").val('Reset');
                    }
                },
                error : function (jqXHR, textStatus, errorThrown) {
                       console.log(errorThrown);
                }
            });
        }
    });

    
    
})(jQuery)