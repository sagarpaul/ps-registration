<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
class Ps_Registration_Shortcode{

    /**
     * Holds the class object.
     *
     * @since 1.0
     *
     */
    public static $_instance;
    /**
     * Plugin Name
     *
     * @since 1.0
     *
     */
    public $plugin_name = 'Front-End User Login and Registration';

    /**
     * Plugin Version
     *
     * @since 1.0
     *
     */

    public $plugin_version = '1.0';

    /**
     * Plugin File
     *
     * @since 1.0
     *
     */

    public $file = __FILE__;

    
    public $base;


    /**
     * Load Construct
     * @since 1.0
     *
     */


    public function __construct(){
        $this->ps_init_shortcode();
    }

    public function ps_init_shortcode(){
    	add_shortcode('ps_registration', array($this,'ps_show_shortcode'));
    }

    public function ps_show_shortcode($atts, $content = NULL){

    	extract(shortcode_atts(
          array(
            'name' => '',
          ), $atts)
        );

        $this->base = Ps_Setting::ps_get_instance();
        
        ob_start();
        ?>
        <section>
            <div id="ps-wrapper">
                <?php
                    if(!is_user_logged_in()){
                        
                        $show_password = $this->base->ps_get_option( 'ps_password', 'ps_feild_setting','yes');
                        $show_first_name = $this->base->ps_get_option( 'ps_first_name', 'ps_feild_setting','');
                        $first_name_required = '';
                        $show_last_name = $this->base->ps_get_option( 'ps_last_name', 'ps_feild_setting','');
                        $last_name_required = '';

                        if(is_array($show_first_name) && !empty($show_first_name)){
                            $first_name_required = (isset($show_first_name['ps-required'])) ? $show_first_name['ps-required'] : '' ;
                            $show_first_name = isset($show_first_name['yes']) ? 'yes' : '';
                        }

                        if(is_array($show_last_name) && !empty($show_last_name)){
                            $last_name_required = (isset($show_last_name['ps-required']) ? 'ps-required' : '');
                            $show_last_name = (isset($show_last_name['yes']) ? 'yes' : '');
                        }

                        $redirect = $this->base->ps_get_option( 'ps_register_redirect', 'ps_basic_setting','');
                        $login_redirect = $this->base->ps_get_option( 'ps_login_redirect', 'ps_basic_setting','');
                        $logout_redirect = $this->base->ps_get_option( 'ps_logut_redirect', 'ps_basic_setting','');

                        require dirname($this->file). '/template/template.php';
                    }
                    else{
                        $logout_redirect = $this->base->ps_get_option( 'ps_logut_redirect', 'ps_basic_setting','');
                        $user_details =  new WP_User(get_current_user_id());
                        ?>
                        <div class="login_content">
                            <?php echo get_avatar( get_the_author_meta( $user_details->ID ), 70 ); ?>
                            <h1 id="ps_login_success"><?php echo esc_html__('Hi','ps-registration'); ?> <?php echo esc_html($user_details->display_name);?><br><a class="ps_logout" href="<?php echo wp_logout_url(get_permalink($logout_redirect)); ?>"><?php echo esc_html__('Logout','ps-registration'); ?></a></h1>
                        </div>
                        <?php
                    }
                    
                    
                ?>
            </div>
        </section>
        <?php
        return ob_get_clean();
    }
    
	public static function ps_get_instance() {
        if (!isset(self::$_instance)) {
            self::$_instance = new Ps_Registration_Shortcode();
        }
        return self::$_instance;
    }

}

Ps_Registration_Shortcode::ps_get_instance();

?>