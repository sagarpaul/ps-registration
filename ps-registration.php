<?php
/**
 * Plugin Name: Front-End User Login and Registration
 * Plugin URI:http://sagarpaul.com
 * Description: Front-End User Login and Registration. User this shortcode [ps_registration]
 * Author: Sagar Paul
 * Author URI: http://sagarpaul.com
 * Version:1.0
 */
if ( ! defined( 'ABSPATH' ) ) exit;

class Ps_Registration{
	/**
     * Holds the class object.
     *
     * @since 1.0
     *
     */
	public static $_instance;
	/**
     * Plugin Name
     *
     * @since 1.0
     *
     */
	public $plugin_name = 'Front-End User Login and Registration';

	/**
     * Plugin Version
     *
     * @since 1.0
     *
     */

	public $plugin_version = '1.0';

	/**
     * Plugin File
     *
     * @since 1.0
     *
     */

	public $file = __FILE__;

	
    public $base;


	/**
     * Load Construct
     * @since 1.0
     *
     */

	public function __construct(){
		$this->ps_plugin_init();
	}

	/**
     * Plugin Initialization
     * @since 1.0
     *
     */

	public function ps_plugin_init(){

        add_action( 'wp_enqueue_scripts', array( $this, 'ps_enqueue_scripts'));

        require_once plugin_dir_path($this->file). 'inc/ps-functions.php';
        require_once plugin_dir_path($this->file). 'inc/setting-class.php';
        require_once plugin_dir_path($this->file). 'inc/ps-setting.php';
        require_once plugin_dir_path($this->file). 'shortcode.php';
       
	}

    /**
     * Enqueue Script
     * @since 1.0
     *
     */
    public function ps_enqueue_scripts(){
        
        wp_enqueue_style('ps-registration-style', plugin_dir_url($this->file) . 'assets/css/style.css');
        wp_enqueue_script('ps-registration-ajax', plugin_dir_url($this->file) . 'assets/js/script.js', array('jquery'), '', true);
        
        /*Ajax Call*/
        $params = array(
            'ajaxurl' => admin_url('admin-ajax.php'),
            'ajax_nonce' => wp_create_nonce('ps_security_check'),
        );
        wp_localize_script('ps-registration-ajax', 'ps_check_obj', $params);
    }

	public static function ps_get_instance() {
        if (!isset(self::$_instance)) {
            self::$_instance = new Ps_Registration();
        }
        return self::$_instance;
    }
}

$Ps_Registration = Ps_Registration::ps_get_instance();